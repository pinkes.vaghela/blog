<?php

namespace App\Http\Controllers;
use App\Models\Post;
use App\Models\Vote;
use App\Models\PostImage;
use App\Http\Requests\PostRequest;
use Illuminate\Http\Request;
use File;
use Auth;

class PostController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function create()
    {
        return view('post');
    }

    public function store(PostRequest $request)
    {
        try {
            $post =  new Post;
            $post->title = $request->get('title');
            $post->body = $request->get('body');
            $post->save();

            if ($request->hasFile('image')) {
                $image = $request->file('image');
                foreach ($image as $files) {
                    if (! File::exists(public_path()."/post")) {
                        File::makeDirectory(public_path()."/post");
                    }

                    $destinationPath = 'public/post/';
                    $file_name = time() . "." . $files->getClientOriginalExtension();
                    $files->move($destinationPath, $file_name);

                    $postImage = new PostImage;
                    $postImage->post_id = $post->id;
                    $postImage->image = $file_name;
                    $postImage->save();
                }
            }
            return redirect()->route('dashboard')->with('success', 'Post has been successfully created');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Error occurred, Please try again later');
        }
    }

    public function index()
    {
        $posts = Post::select('id', 'title')->get();
        foreach ($posts as $key => $value) {
            $vote = $value->votes()->where('user_id', Auth::user()->id)->first();
            $value['vote'] = $vote->vote ?? 0;
        }
        
        return view('index', compact('posts'));
    }

    public function show($id)
    {
        $post = Post::find($id);
        return view('show', compact('post'));
    }

    public function edit($id)
    {
        $post = Post::find($id);
        return view('edit', compact('post'));
    }

    public function update(PostRequest $request, $id)
    {
        try {
            $post =  Post::find($id);
            $post->title = $request->get('title');
            $post->body = $request->get('body');
            $post->update();

            if ($request->hasFile('image')) {
                $image = $request->file('image');
                foreach ($image as $files) {
                    if (! File::exists(public_path()."/post")) {
                        File::makeDirectory(public_path()."/post");
                    }

                    PostImage::where('post_id',$id)->delete();
                    $destinationPath = 'public/post/';
                    $file_name = time() . "." . $files->getClientOriginalExtension();
                    $files->move($destinationPath, $file_name);

                    $postImage = new PostImage;
                    $postImage->post_id = $post->id;
                    $postImage->image = $file_name;
                    $postImage->save();
                }
            }
            return redirect()->route('dashboard')->with('success', 'Post has been successfully updated');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Error occurred, Please try again later');
        }
    }

    public function vote(Request $request)
    {
        $vote = Vote::where([['user_id',Auth::user()->id],['post_id',$request->post_id]])->first();
        if($vote){
            $vote->vote = $request->vote;
            $vote->update();
        }else{
            $voteObject = new Vote;
            $voteObject->user_id = Auth::user()->id;
            $voteObject->post_id = $request->post_id;
            $voteObject->vote = $request->vote;
            $voteObject->save();
        }

        return 1;
    }
}
