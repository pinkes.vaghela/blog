<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
use App\Models\Post;
use File;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $comment = new Comment;
        $comment->body = $request->get('comment_body');
        if ($request->hasFile('image')) {
            $files = $request->file('image');
            if (! File::exists(public_path()."/comment")) {
                File::makeDirectory(public_path()."/comment");
            }
            $destinationPath = 'public/comment/';
            $file_name = time() . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $file_name);
            $comment->image = $file_name;
        }
        $comment->user()->associate($request->user());
        $post = Post::find($request->get('post_id'));
        $post->comments()->save($comment);

        return back();
    }

    public function replyStore(Request $request)
    {
        $reply = new Comment();
        $reply->body = $request->get('comment_body');
        if ($request->hasFile('image')) {
            $files = $request->file('image');
            if (! File::exists(public_path()."/comment")) {
                File::makeDirectory(public_path()."/comment");
            }
            $destinationPath = 'public/comment/';
            $file_name = time() . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $file_name);
            $reply->image = $file_name;
        }
        $reply->user()->associate($request->user());
        $reply->parent_id = $request->get('comment_id');
        $post = Post::find($request->get('post_id'));

        $post->comments()->save($reply);

        return back();

    }

    public function delete($id)
    {
        Comment::where('id',$id)->delete();
        Comment::where('parent_id',$id)->delete();
        return back();
    }
}
