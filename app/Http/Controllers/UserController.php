<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Models\User;
use Auth;

class UserController extends Controller
{
    //

    public function showLoginPage() {

        return view('login');
    }

    public function showRegisterPage() {
        return view('register');

    }

    public function register(RegisterRequest $request) {
        try {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();
    
            return redirect()->back()->with('success', 'User registered successfully');

        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Error occurred, Please try again later');
        }
    }

    public function login(LoginRequest $request) {

        if(Auth::attempt(array('email' => $request->email, 'password'=> $request->password))) {
            return redirect()->route('dashboard');
        }        
        return redirect()->route('login')->with('error', 'Login email and password do not match with your database credentials');

    }

    
    public function logout() {
        Auth::logout();
        return redirect(route('login'));
    }
}
