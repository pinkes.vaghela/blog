<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }
    public function images()
    {
        return $this->hasMany(PostImage::class, 'post_id');
    }
    public function votes()
    {
        return $this->hasMany(Vote::class, 'post_id');
    }
}
