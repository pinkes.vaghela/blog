<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', [ UserController::class, 'showLoginPage' ])->name('login');
Route::get('/register', [ UserController::class, 'showRegisterPage' ])->name('register');
Route::post('/register', [ UserController::class, 'register' ])->name('register.submit');
Route::post('/login', [ UserController::class, 'login' ])->name('login.submit');

Route::group([ 'middleware' => ['auth'] ], function() {
    Route::get('/', [ PostController::class, 'index' ])->name('dashboard');
    Route::get('/post/create', [ PostController::class, 'create' ])->name('post.create');
    Route::post('/post/store', [ PostController::class, 'store' ])->name('post.store');
    Route::get('/post/show/{id}', [ PostController::class, 'show' ])->name('post.show');
    Route::get('/post/edit/{id}', [ PostController::class, 'edit' ])->name('post.edit');
    Route::post('/post/update/{id}', [ PostController::class, 'update' ])->name('post.update');
    Route::post('/post/vote', [ PostController::class, 'vote' ])->name('post.vote');

    Route::post('/comment/store',  [ CommentController::class, 'store' ])->name('comment.add');
	Route::post('/reply/store', [ CommentController::class, 'replyStore' ])->name('reply.add');
	Route::get('/comment/delete/{id}', [ CommentController::class, 'delete' ])->name('comment.delete');

    Route::post('/logout', [ UserController::class, 'logout' ])->name('logout');

});