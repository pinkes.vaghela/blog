@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if(session('success'))
                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  {{ session('success') }}
                </div>
            @endif
            <table class="table table-striped">
                <thead>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Vote</th>
                    <th>Action</th>
                </thead>
                <tbody>
                @forelse($posts as $post)
                <tr>
                    <td>{{ $post->id }}</td>
                    <td>{{ $post->title }}</td>
                    <td>
                        <i class="fa vote @if($post->vote && $post->vote ==1) fa-thumbs-up @else fa-thumbs-o-up @endif" data="@if($post->vote){{$post->vote}}@else{{0}}@endif" post_id="{{$post->id}}"></i>
                    </td>
                    <td>
                        <a href="{{ route('post.show', $post->id) }}" class="btn btn-primary">Show Post</a>
                        <a href="{{ route('post.edit', $post->id) }}" class="btn btn-primary">Edit Post</a>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="3" style="text-align: center;">No data founds</td>
                </tr>
                @endforelse
                </tbody>

            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('body').on('click', '.vote', function() {
            var data = $(this).attr('data');
            var current=$(this);
            var post_id = $(this).attr('post_id');
            var vote = 1;
            if(data ==1){
                vote = 2;
            }else if(vote ==2){
                vote = 1;
            } 
            $.ajax({
              url:"{{route('post.vote')}}",
              type: "POST",
              data: {
                vote: vote,
                post_id: post_id,
                _token: '{{csrf_token()}}' 
              },
              dataType : 'json',
              success: function(result){
                location.reload();
              }
            });
        }); 
    });
</script>

@endsection
