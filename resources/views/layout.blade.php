
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Task</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <script src="{{ asset('css/jquery-ui.css') }}"></script>

    <style>
      #draggable { width: 150px; height: 150px; padding: 0.5em; }
      .singleSection {
        width: auto;
        height: auto;
        cursor: pointer;
      }
      .storageGroup {
        width:1000px;
        height:500px;
        border:1px solid #000;
        overflow:scroll;
        display:flex;

      }
      </style>

  </head>

  <body>

    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 class="my-0 mr-md-auto font-weight-normal"><a href="{{ route('dashboard') }}">Company name</a></h5>
      @if(Auth::user())
        <nav class="my-2 my-md-0 mr-md-3">
          <a class="p-2 text-dark" href="{{ route('changePassword') }}">Change Password</a>
        </nav>
        <a class="btn btn-outline-danger mr-2" href="{{ route('logout') }}">Sign Out</a>
      @endif

      @if(!Auth::user())

        <a class="btn btn-outline-primary mr-2" href="{{ route('register') }}">Sign up</a>
        <a class="btn btn-outline-primary mr-2" href="{{ route('login') }}">Sign In</a>
      @endif
      
    </div>

<div class="container" >
  @if(session('success'))
    <div class="alert alert-success" role="alert">
      {{ session('success') }}
    </div>
  @endif

  @if(session('error'))
    <div class="alert alert-danger" role="alert">
      {{ session('error') }}
    </div>
  @endif
</div>

    @yield('body')


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="{{ asset('js/jquery-1.12.4.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery-ui.js') }}"></script>
  </body>

  @yield('script')

</html>
