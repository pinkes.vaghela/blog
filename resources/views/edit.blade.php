@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if(session('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ session('error') }}
              </div>
            @endif
            <div class="card">
                <div class="card-header">Edit Post</div>
                <div class="card-body">
                    <form method="post" action="{{ route('post.update',$post->id) }}" enctype="multipart/form-data">
                        <div class="form-group">
                            @csrf
                            <label class="label">Post Title: </label>
                            <input type="text" name="title" class="form-control" value="{{$post->title}}"/>
                            @if($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="label">Post Body: </label>
                            <textarea name="body" rows="10" cols="30" class="form-control">{{ $post->body }}</textarea>
                            @if($errors->has('body'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('body') }}</strong>
                                </span>

                            @endif
                        </div>

                        <div class="form-group">
                            <label class="label">Files: </label>
                            <input type="file" name="image[]" multiple="multiple" class="form-control">
                            @if($errors->has('image.*'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('image.*') }}</strong>
                                </span>
                            @endif

                            
                           @forelse($post->images as $imageData)
                                <a href="{{asset('public/post/'.$imageData->image)}}" target="_blank">Preview File</a> <br>
                            @empty
                           @endforelse
                            
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-success" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
