 @foreach($comments as $comment)
    <div class="display-comment">
        <strong>{{ $comment->user->name }}</strong>
        <p>{{ $comment->body }}</p>
        @if($comment->image)<a href="{{asset('public/comment/'.$comment->image)}}" target="_blank">File Preview</a>@endif <br>
        @if($comment->user->id == Auth::user()->id) <a href="{{route('comment.delete',$comment->id)}}">Delete Comment</a> @endif
        <form method="post" action="{{ route('reply.add') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <input type="text" name="comment_body" class="form-control" />
                <input type="hidden" name="post_id" value="{{ $post_id }}" />
                <input type="hidden" name="comment_id" value="{{ $comment->id }}" />
            </div>
            <div class="form-group">
                <input type="file" name="image" class="form-control" />
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-warning" value="Reply" />
            </div>
        </form>
        @include('comment_replies', ['comments' => $comment->replies])
    </div>
@endforeach